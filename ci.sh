#!/bin/bash

if [ "$TRAVIS_PULL_REQUEST" = "true" ] || [ "$TRAVIS_BRANCH" != "master" ]; then
  TAG="${TRAVIS_TAG:-release-$TRAVIS_BRANCH}"
else
  TAG="${TRAVIS_TAG:-latest}"
fi
echo $DOCKER_PASSWORD | docker login -u $DOCKER_USERNAME --password-stdin &> /dev/null

docker buildx build \
     --progress plain \
    --platform=linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6 \
    -t $DOCKER_REPO:$TAG \
    --push \
    .
